package com.spring.deboragomez.species.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * The Animals model.
 * @author Débora Gómez Bertoli.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "favourites",
        "others"
})
public class Animals {

    @JsonProperty("favourites")
    private List<Animal> favourites = new ArrayList<Animal>();
    @JsonProperty("others")
    private List<Animal> others = new ArrayList<Animal>();

    /**
     * @return The favourites
     */
    @JsonProperty("favourites")
    public List<Animal> getFavourites() {
        return favourites;
    }

    /**
     * @param favourites The favourites
     */
    @JsonProperty("favourites")
    public void setFavourites(List<Animal> favourites) {
        this.favourites = favourites;
    }

    /**
     * @return The others
     */
    @JsonProperty("others")
    public List<Animal> getOthers() {
        return others;
    }

    /**
     * @param others The others
     */
    @JsonProperty("others")
    public void setOthers(List<Animal> others) {
        this.others = others;
    }

}
