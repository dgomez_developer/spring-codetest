package com.spring.deboragomez.species.presentation.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.view.Menu;
import android.view.MenuItem;

import com.spring.deboragomez.species.R;
import com.spring.deboragomez.species.presentation.SpeciesApplication;
import com.spring.deboragomez.species.presentation.di.HasComponent;
import com.spring.deboragomez.species.presentation.di.components.ApplicationComponent;
import com.spring.deboragomez.species.presentation.di.components.DaggerSpeciesComponent;
import com.spring.deboragomez.species.presentation.di.components.SpeciesComponent;
import com.spring.deboragomez.species.presentation.view.adapters.SpeciesCardsAdapter;

import butterknife.ButterKnife;

public class SpeciesActivity extends AppCompatActivity implements SpeciesListenerView, HasComponent<SpeciesComponent> {

    private FragmentManager fragmentManager;

    private SpeciesComponent speciesComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initializeInjector();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getApplicationComponent().inject(this);
        speciesComponent.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        SpeciesFragment fragment = SpeciesFragment.newInstance();
        fragment.registerSpicesListenerView(this);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.activity_main_content_framelayout, fragment, SpeciesFragment.TAG);
        transaction.commit();
    }


    private void initializeInjector() {
        this.speciesComponent = DaggerSpeciesComponent.builder()
                .applicationComponent(getApplicationComponent())
                .build();
    }

    /**
     * Get the Main Application component for dependency injection.
     */
    protected ApplicationComponent getApplicationComponent() {
        return ((SpeciesApplication) getApplication()).getApplicationComponent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSpeciesItemSelected(SpeciesCardsAdapter.ViewHolder holder, String species) {

        SpeciesDetailFragment fragment = SpeciesDetailFragment.newInstance(species);
        fragment.setSharedElementEnterTransition(new DetailsTransition());
        fragment.setEnterTransition(new Fade());
        fragment.setSharedElementReturnTransition(new DetailsTransition());

        SpeciesFragment currentFragment = (SpeciesFragment) fragmentManager.findFragmentByTag(SpeciesFragment.TAG);
        currentFragment.setExitTransition(new Fade());

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addSharedElement(holder.image, "animalImage");
        transaction.replace(R.id.activity_main_content_framelayout, fragment, SpeciesDetailFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    @Override
    public SpeciesComponent getComponent() {
        return speciesComponent;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        super.onSaveInstanceState(savedInstanceState);
    }

}
