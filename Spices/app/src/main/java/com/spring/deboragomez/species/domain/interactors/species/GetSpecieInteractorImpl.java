package com.spring.deboragomez.species.domain.interactors.species;

import com.spring.deboragomez.species.domain.executors.PostExecutionThread;
import com.spring.deboragomez.species.domain.executors.ThreadExecutor;
import com.spring.deboragomez.species.domain.interactors.BaseInteractor;
import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.domain.repository.SpeciesRepository;

import javax.inject.Inject;

/**
 * Get a concrete specie interactor implementation.
 *
 * @author Débora Gómez Bertoli.
 */
public class GetSpecieInteractorImpl extends BaseInteractor implements GetSpecieInteractor {

    private SpeciesRepository repository;
    private Callback callback;
    private String specie;

    /**
     * Constructor.
     *
     * @param threadExecutor      The thread executor.
     * @param postExecutionThread The post executon thread.
     * @param repository          The species repository.
     */
    @Inject
    public GetSpecieInteractorImpl(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, SpeciesRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    public void execute(String specie, Callback callback) {
        this.callback = callback;
        this.specie = specie;
        execute();
    }

    @Override
    public void run() {

        repository.getSpecie(specie, new SpeciesRepository.SpecieCallback() {
            @Override
            public void onSuccess(final Animal animal) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        if (animal == null) {
                            callback.onError(new Exception("No animal found for species :" + specie));
                        } else {
                            callback.onSuccess(animal);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception e) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onError(e);
                    }
                });
            }
        });

    }
}
