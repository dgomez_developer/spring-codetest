package com.spring.deboragomez.species.data.datasource.network.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * This class initializes the retrofit service and configures it.
 *
 * @author Débora Gómez Bertoli.
 */
public class RetrofitService {


    private static final String BASE_URL = "http://files.ilicco.com/digitaslbi/recruitment/";

    private static Retrofit buildJsonRetrofitService() {
        return new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

    }

    /**
     * Creates the JSON API.
     *
     * @return json service instance.
     */
    public static JsonService getJsonService() {

        return buildJsonRetrofitService().create(JsonService.class);
    }

}

