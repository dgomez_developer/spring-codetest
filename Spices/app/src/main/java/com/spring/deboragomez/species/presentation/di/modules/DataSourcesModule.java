package com.spring.deboragomez.species.presentation.di.modules;

import com.spring.deboragomez.species.data.datasource.AnimalsDataSource;
import com.spring.deboragomez.species.data.datasource.memory.AnimalsMemoryDataSource;
import com.spring.deboragomez.species.data.datasource.network.AnimalsNetworkDataSource;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * A module that provides objects of data sources.
 *
 * @author Débora Gómez Bertoli.
 */
@Module(includes = {ApplicationModule.class})
public class DataSourcesModule {

    @Provides
    @Named("memory")
    public AnimalsDataSource provideUserMemoryDataSource(AnimalsMemoryDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Named("network")
    public AnimalsDataSource provideUserNetworkDataSource(AnimalsNetworkDataSource dataSource) {
        return dataSource;
    }

}
