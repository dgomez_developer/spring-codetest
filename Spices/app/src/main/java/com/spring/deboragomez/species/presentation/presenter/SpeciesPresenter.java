package com.spring.deboragomez.species.presentation.presenter;

import com.spring.deboragomez.species.presentation.view.SpeciesView;

/**
 * The species presenter interface.
 *
 * @author Débora Gómez Bertoli.
 */
public interface SpeciesPresenter {

    /**
     * Initializes the screen information with the species.
     */
    void onCreate();

    /**
     * Sets the UI listener.
     *
     * @param speciesFragment The listener
     */
    void setView(SpeciesView speciesFragment);
}
