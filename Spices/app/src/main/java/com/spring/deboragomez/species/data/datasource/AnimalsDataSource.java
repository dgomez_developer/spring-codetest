package com.spring.deboragomez.species.data.datasource;

import com.spring.deboragomez.species.domain.model.Species;

/**
 * Interface for application's data sources.
 *
 * @author Débora Gómez Bertoli.
 */
public interface AnimalsDataSource {

    /**
     * Callback for data sources operations.
     */
    interface Callback {

        /**
         * Called when the request finishes.
         *
         * @param animals The animals content.
         */
        void onSuccess(Species animals);

        /**
         * Called when an error happens.
         *
         * @param e The error.
         */
        void onError(Exception e);
    }

    /**
     * Stores the species information.
     *
     * @param species  The species information.
     * @param callback The callback.
     */
    void setSpecies(Species species, Callback callback);

    /**
     * Gets the species information.
     *
     * @param callback The callback.
     */
    void getSpecies(Callback callback);

}
