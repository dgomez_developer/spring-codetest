package com.spring.deboragomez.species.presentation.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.spring.deboragomez.species.R;
import com.spring.deboragomez.species.presentation.di.components.SpeciesComponent;
import com.spring.deboragomez.species.presentation.model.SpeciesCardView;
import com.spring.deboragomez.species.presentation.presenter.SpeciesPresenter;
import com.spring.deboragomez.species.presentation.view.adapters.SpeciesCardsAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing the species details view.
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesFragment extends BaseFragment implements SpeciesView, SwipeRefreshLayout.OnRefreshListener, SpeciesCardsAdapter.ItemListListener {


    public static final String TAG = "SpeciesFragment";
    @Bind(R.id.fragment_spices_container_recyclerview)
    RecyclerView cardsContainer;

    @Bind(R.id.fragment_spices_swiperefresh)
    SwipeRefreshLayout refreshLayout;

    @Inject
    SpeciesPresenter speciesPresenter;

    private SpeciesListenerView speciesListenerView;

    private SpeciesCardsAdapter adapter;

    /**
     * Constructor.
     */
    public SpeciesFragment() {

    }

    /**
     * Use this factory method to create a new instance of this fragment using the provided parameters.
     *
     * @return A new instance of fragment SpeciesFragment.
     */
    public static SpeciesFragment newInstance() {
        SpeciesFragment fragment = new SpeciesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(SpeciesComponent.class).inject(this);
        speciesPresenter.setView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_species, container, false);
        ButterKnife.bind(this, view);

        cardsContainer.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        cardsContainer.setLayoutManager(manager);
        refreshLayout.setOnRefreshListener(this);
        setRetainInstance(true);
        return view;
    }

    /**
     * Registers the listener for the list of animals displayed.
     *
     * @param speciesListenerView The listener.
     */
    public void registerSpicesListenerView(SpeciesListenerView speciesListenerView) {
        this.speciesListenerView = speciesListenerView;
    }

    @Override
    public void onActivityCreated(Bundle saveInstanceBundle) {
        super.onActivityCreated(saveInstanceBundle);
        if (saveInstanceBundle != null) {
            refreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    refreshLayout.setRefreshing(true);
                }
            });
        }
        speciesPresenter.onCreate();
    }

    @Override
    public void onRefresh() {
        cardsContainer.setAdapter(null);
        speciesPresenter.onCreate();
    }

    @Override
    public void onItemClick(SpeciesCardsAdapter.ViewHolder holder, int position) {
        SpeciesCardView card = adapter.getItemAt(position);
        speciesListenerView.onSpeciesItemSelected(holder, card.getName());
    }

    @Override
    public void showSpicesList(List<SpeciesCardView> items) {
        cardsContainer.setAdapter(null);
        refreshLayout.setRefreshing(false);
        this.adapter = new SpeciesCardsAdapter(getActivity(), items, this);
        cardsContainer.setAdapter(adapter);
    }

    @Override
    public void showError(int errorId) {
        refreshLayout.setRefreshing(false);
        Snackbar.make(getView(), getString(errorId), Snackbar.LENGTH_LONG).show();
    }
}
