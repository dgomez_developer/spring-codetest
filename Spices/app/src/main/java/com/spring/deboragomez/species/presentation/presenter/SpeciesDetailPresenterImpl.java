package com.spring.deboragomez.species.presentation.presenter;

import android.util.Log;

import com.spring.deboragomez.species.R;
import com.spring.deboragomez.species.domain.interactors.species.GetSpecieInteractor;
import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.presentation.model.mapper.SpeciesViewMapper;
import com.spring.deboragomez.species.presentation.view.SpeciesDetailView;

import javax.inject.Inject;

/**
 * The species details screen presenter implementation.
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesDetailPresenterImpl implements SpeciesDetailPresenter {

    private static final String TAG = "SpeciesDetailPresenter";

    private SpeciesDetailView view;

    private final GetSpecieInteractor interactor;

    private final SpeciesViewMapper mapper;

    /**
     * Constructor.
     *
     * @param interactor Get species detail interactor.
     * @param mapper     The model view mapper.
     */
    @Inject
    public SpeciesDetailPresenterImpl(GetSpecieInteractor interactor, SpeciesViewMapper mapper) {
        this.interactor = interactor;
        this.mapper = mapper;
    }


    @Override
    public void onCreate(String species) {

        interactor.execute(species, new GetSpecieInteractor.Callback() {
            @Override
            public void onSuccess(Animal animal) {
                Log.d(TAG, "Animal received");
                mapper.fixImageUrlIfNeeded(animal);
                view.showAnimalDetails(animal);
            }

            @Override
            public void onError(Exception e) {
                Log.e(TAG, "Error: " + e.getMessage());
                if(e.getMessage().equals("NO_NETWORK_CONNECTION")){
                    view.showError(R.string.network_error);
                } else{
                    view.showError(R.string.generic_error);
                }
            }
        });

    }

    @Override
    public void setView(SpeciesDetailView speciesDetailFragment) {
        this.view = speciesDetailFragment;
    }
}
