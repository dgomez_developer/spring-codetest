package com.spring.deboragomez.species.presentation.di.modules;

import android.content.Context;

import com.spring.deboragomez.species.data.datasource.network.retrofit.JsonService;
import com.spring.deboragomez.species.data.datasource.network.retrofit.RetrofitService;
import com.spring.deboragomez.species.data.executors.RequestExecutor;
import com.spring.deboragomez.species.domain.executors.PostExecutionThread;
import com.spring.deboragomez.species.domain.executors.ThreadExecutor;
import com.spring.deboragomez.species.presentation.SpeciesApplication;
import com.spring.deboragomez.species.presentation.executors.UiThread;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * A module that provides objects with application lifecycle.
 * @author Débora Gómez Bertoli.
 */
@Module(includes = {RepositoryModule.class})
public class ApplicationModule {

    private final SpeciesApplication application;

    public ApplicationModule(SpeciesApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    public ThreadExecutor provideJobExecutor(RequestExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    public PostExecutionThread providePostExecutionThread(UiThread postExecutionThread) {
        return postExecutionThread;
    }

    @Provides
    @Singleton
    JsonService providesJsonParser() {
        return RetrofitService.getJsonService();
    }

}
