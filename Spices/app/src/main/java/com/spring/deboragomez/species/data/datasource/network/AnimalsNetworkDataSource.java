package com.spring.deboragomez.species.data.datasource.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.spring.deboragomez.species.data.datasource.AnimalsDataSource;
import com.spring.deboragomez.species.data.datasource.network.retrofit.JsonService;
import com.spring.deboragomez.species.domain.model.Species;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Data source that gets the Animals information from the server.
 *
 * @author Débora Gómez Bertoli.
 */
public class AnimalsNetworkDataSource implements AnimalsDataSource {


    private static final String TAG = "NetworkDataSource";

    private JsonService jsonService;

    private Context context;
    /**
     * Constructor
     *
     * @param jsonService The Json service for the requests.
     */
    @Inject
    public AnimalsNetworkDataSource(JsonService jsonService, Context context) {
        this.jsonService = jsonService;
        this.context = context;
    }


    @Override
    public void setSpecies(Species species, Callback callback) {
        throw new UnsupportedOperationException("Operation not supported for this data source");
    }

    @Override
    public void getSpecies(Callback callback) {

        if(!isOnline()){
            callback.onError(new Exception("NO_NETWORK_CONNECTION"));
            return;
        }

        try {
            Call<Species> spicesCall = jsonService.getSpecies();
            Response<Species> response = spicesCall.execute();
            ResponseBody errorBody = response.errorBody();
            if (errorBody == null) {
                Species species = response.body();
                callback.onSuccess(species);
            } else {
                callback.onError(new Exception("An error occured"));
            }
        } catch (Exception e) {
            Log.e(TAG, "Error: " + e.toString());
            callback.onError(e);
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
