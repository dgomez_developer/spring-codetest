package com.spring.deboragomez.species.presentation.di.modules;

import com.spring.deboragomez.species.data.SpeciesRepositoryImpl;
import com.spring.deboragomez.species.domain.repository.SpeciesRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * A module that provides objects of repository.
 * @author Débora Gómez Bertoli.
 */
@Module(includes = DataSourcesModule.class)
public class RepositoryModule {

    @Provides
    @Singleton
    public SpeciesRepository provideSpeciesRepository(SpeciesRepositoryImpl repository) {
        return repository;
    }
}
