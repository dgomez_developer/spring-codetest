package com.spring.deboragomez.species.presentation.di.components;

import android.content.Context;

import com.spring.deboragomez.species.data.datasource.network.retrofit.JsonService;
import com.spring.deboragomez.species.domain.executors.PostExecutionThread;
import com.spring.deboragomez.species.domain.executors.ThreadExecutor;
import com.spring.deboragomez.species.domain.repository.SpeciesRepository;
import com.spring.deboragomez.species.presentation.SpeciesApplication;
import com.spring.deboragomez.species.presentation.di.modules.ApplicationModule;
import com.spring.deboragomez.species.presentation.di.modules.RepositoryModule;
import com.spring.deboragomez.species.presentation.view.SpeciesActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * A component whose lifetime is the life of the application.
 *
 * @author Débora Gómez Bertoli.
 */
@Singleton
@Component(modules = {ApplicationModule.class, RepositoryModule.class})

public interface ApplicationComponent {

    void inject(SpeciesActivity baseActivity);

    void inject(SpeciesApplication woodyApplication);

    Context context();

    ThreadExecutor threadExecutor();

    PostExecutionThread postExecutionThread();

    SpeciesRepository speciesRepository();

    JsonService jsonService();

}
