package com.spring.deboragomez.species.presentation.presenter;

import android.util.Log;

import com.spring.deboragomez.species.R;
import com.spring.deboragomez.species.domain.interactors.species.GetSpeciesInteractor;
import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.presentation.model.mapper.SpeciesViewMapper;
import com.spring.deboragomez.species.presentation.view.SpeciesView;

import java.util.List;

import javax.inject.Inject;

/**
 * The species presenter implementation.
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesPresenterImpl implements SpeciesPresenter {

    private static final String TAG = "SpeciesPresenterImpl";
    private GetSpeciesInteractor getSpeciesInteractor;
    private SpeciesView view;
    private SpeciesViewMapper mapper;

    /**
     * Constructor.
     *
     * @param getSpeciesInteractor Get species interactor
     * @param mapper               The mapper for view model.
     */
    @Inject
    public SpeciesPresenterImpl(GetSpeciesInteractor getSpeciesInteractor, SpeciesViewMapper mapper) {
        this.getSpeciesInteractor = getSpeciesInteractor;
        this.mapper = mapper;
    }


    @Override
    public void onCreate() {

        getSpeciesInteractor.execute(new GetSpeciesInteractor.Callback() {
            @Override
            public void onSuccess(List<Animal> favAnimals, List<Animal> otherAnimals) {
                Log.d(TAG, "Animals received");
                view.showSpicesList(mapper.map(favAnimals, otherAnimals));
            }

            @Override
            public void onError(Exception e) {
                Log.e(TAG, "An error occured: " + e.toString());
                if(e.getMessage().equals("NO_NETWORK_CONNECTION")){
                    view.showError(R.string.network_error);
                } else{
                    view.showError(R.string.generic_error);
                }
            }
        });

    }

    @Override
    public void setView(SpeciesView speciesFragment) {
        this.view = speciesFragment;
    }
}
