package com.spring.deboragomez.species.presentation.view;

import com.spring.deboragomez.species.presentation.view.adapters.SpeciesCardsAdapter;

/**
 * Listener for the list of animals.
 *
 * @author Débora Gómez Bertoli.
 */
public interface SpeciesListenerView {

    /**
     * Called when an item within the list has been selected.
     *
     * @param holder The view holder.
     * @param species The selected species.
     */
    void onSpeciesItemSelected(SpeciesCardsAdapter.ViewHolder holder, String species);

}
