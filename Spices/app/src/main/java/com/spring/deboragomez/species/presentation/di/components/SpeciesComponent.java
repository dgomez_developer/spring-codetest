package com.spring.deboragomez.species.presentation.di.components;

import com.spring.deboragomez.species.presentation.di.PerActivity;
import com.spring.deboragomez.species.presentation.di.modules.SpeciesModule;
import com.spring.deboragomez.species.presentation.view.SpeciesActivity;
import com.spring.deboragomez.species.presentation.view.SpeciesDetailFragment;
import com.spring.deboragomez.species.presentation.view.SpeciesFragment;

import dagger.Component;

/**
 * A Component with cycle life of the Activity. This component is to inject the Species.
 *
 * @author Débora Gómez Bertoli.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = SpeciesModule.class)
public interface SpeciesComponent {

    /**
     * Injects SpeciesActivity at Runtime.
     *
     * @param mainActivity Species Activity.
     */
    void inject(SpeciesActivity mainActivity);

    /**
     * Injects SpeciesFragment at Runtime.
     *
     * @param speciesFragment Species Fragment.
     */
    void inject(SpeciesFragment speciesFragment);

    /**
     * Injects SpeciesDetailFragment at Runtime.
     *
     * @param detailsFragment Species Detail Fragment.
     */
    void inject(SpeciesDetailFragment detailsFragment);
}
