package com.spring.deboragomez.species.presentation;

import android.app.Application;

import com.spring.deboragomez.species.presentation.di.components.ApplicationComponent;
import com.spring.deboragomez.species.presentation.di.components.DaggerApplicationComponent;
import com.spring.deboragomez.species.presentation.di.modules.ApplicationModule;

/**
 * Custom application object for dependency injection.
 * @author Débora Gómez Bertoli.
 */
public class SpeciesApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();
    }

    private void initializeInjector() {

        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        this.applicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

}
