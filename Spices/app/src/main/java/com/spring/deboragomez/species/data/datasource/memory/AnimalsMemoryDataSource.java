package com.spring.deboragomez.species.data.datasource.memory;

import com.spring.deboragomez.species.data.datasource.AnimalsDataSource;
import com.spring.deboragomez.species.domain.model.Species;

import javax.inject.Inject;

/**
 * Data Source that caches the information in memory.
 *
 * @author Débora Gómez Bertoli.
 */
public class AnimalsMemoryDataSource implements AnimalsDataSource {

    private Species species;

    @Inject
    public AnimalsMemoryDataSource() {

    }

    @Override
    public void setSpecies(Species species, Callback callback) {
        this.species = species;
        callback.onSuccess(species);
    }

    @Override
    public void getSpecies(Callback callback) {
        callback.onSuccess(species);
    }
}
