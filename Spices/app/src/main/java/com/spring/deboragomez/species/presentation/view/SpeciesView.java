package com.spring.deboragomez.species.presentation.view;

import com.spring.deboragomez.species.presentation.model.SpeciesCardView;

import java.util.List;

/**
 * The interface for species view.
 *
 * @author Débora Gómez Bertoli.
 */
public interface SpeciesView {

    /**
     * Shows all species information on the screen.
     *
     * @param items The list of animals.
     */
    void showSpicesList(List<SpeciesCardView> items);

    /**
     * Shows an error on the screen.
     *
     * @param errorId The resource id message.
     */
    void showError(int errorId);

}
