package com.spring.deboragomez.species.domain.repository;

import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.domain.model.Animals;

/**
 * The Species Repository interface.
 *
 * @author Débora Gómez Bertoli.
 */
public interface SpeciesRepository {

    /**
     * The callback for species operation.
     */
    interface Callback {

        /**
         * Called when the request finishes.
         *
         * @param animals The animals information.
         */
        void onSuccess(Animals animals);

        /**
         * Called when an error happens.
         *
         * @param e The error.
         */
        void onError(Exception e);

    }

    /**
     * The callback for concrete specie operation.
     */
    interface SpecieCallback {

        /**
         * Called when the request finishes.
         *
         * @param animal The animal information.
         */
        void onSuccess(Animal animal);

        /**
         * Called when an error happens.
         *
         * @param e The error.
         */
        void onError(Exception e);

    }

    /**
     * Gets all species information.
     *
     * @param forceRefresh Sets whether getting the information from the server {@code true} or locally {@code false}.
     * @param callback     The callback.
     */
    void getSpecies(boolean forceRefresh, Callback callback);

    /**
     * Gets a concrete species.
     *
     * @param specie   The species.
     * @param callback The callback.
     */
    void getSpecie(String specie, SpecieCallback callback);

}
