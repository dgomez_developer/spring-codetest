package com.spring.deboragomez.species.data;

import com.spring.deboragomez.species.data.datasource.AnimalsDataSource;
import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.domain.model.Animals;
import com.spring.deboragomez.species.domain.model.Species;
import com.spring.deboragomez.species.domain.repository.SpeciesRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * The species repository.
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesRepositoryImpl implements SpeciesRepository {

    private AnimalsDataSource networkDataSource;

    private AnimalsDataSource memoryDataSource;

    /**
     * Constructor,
     *
     * @param memoryDataSource  Memory data source.
     * @param networkDataSource Network data source.
     */
    @Inject
    public SpeciesRepositoryImpl(@Named("memory") AnimalsDataSource memoryDataSource, @Named("network") AnimalsDataSource networkDataSource) {
        this.memoryDataSource = memoryDataSource;
        this.networkDataSource = networkDataSource;
    }

    @Override
    public void getSpecies(boolean forceRefresh, final Callback callback) {

        if (forceRefresh) {

            getSpeciesFromServer(callback);

        } else {

            memoryDataSource.getSpecies(new AnimalsDataSource.Callback() {
                @Override
                public void onSuccess(Species animals) {
                    if (animals != null) {
                        callback.onSuccess(animals.getAnimals());
                    } else {
                        getSpeciesFromServer(callback);
                    }
                }

                @Override
                public void onError(Exception e) {
                    callback.onError(e);
                }
            });

        }
    }

    @Override
    public void getSpecie(final String specie, final SpecieCallback callback) {

        final List<Animal> animalsList = new ArrayList<>();

        getSpecies(false, new Callback() {
            @Override
            public void onSuccess(Animals animals) {
                animalsList.addAll(animals.getFavourites());
                animalsList.addAll(animals.getOthers());
                Animal animalSelected = null;
                for (Animal animal : animalsList) {
                    if (specie.equals(animal.getSpecies())) {
                        animalSelected = animal;
                        break;
                    }
                }
                callback.onSuccess(animalSelected);
            }

            @Override
            public void onError(Exception e) {
                callback.onError(e);
            }
        });

    }

    private void getSpeciesFromServer(final Callback callback) {

        networkDataSource.getSpecies(new AnimalsDataSource.Callback() {
            @Override
            public void onSuccess(Species animals) {
                storeSpecies(animals, callback);
            }

            @Override
            public void onError(Exception e) {
                callback.onError(e);
            }
        });
    }

    private void storeSpecies(Species animals, final Callback callback) {

        memoryDataSource.setSpecies(animals, new AnimalsDataSource.Callback() {
            @Override
            public void onSuccess(Species animals) {
                callback.onSuccess(animals.getAnimals());
            }

            @Override
            public void onError(Exception e) {
                callback.onError(e);
            }
        });
    }
}
