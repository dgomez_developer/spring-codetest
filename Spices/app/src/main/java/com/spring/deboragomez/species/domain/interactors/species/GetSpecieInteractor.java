package com.spring.deboragomez.species.domain.interactors.species;

import com.spring.deboragomez.species.domain.model.Animal;

/**
 * Gets a concrete specie interactor interface.
 *
 * @author Débora Gómez Bertoli.
 */
public interface GetSpecieInteractor {

    /**
     * Callback for the operation.
     */
    interface Callback {

        /**
         * Called when the request finishes.
         *
         * @param animal The animal selected.
         */
        void onSuccess(Animal animal);

        /**
         * Called when an error happens.
         *
         * @param e The error.
         */
        void onError(Exception e);

    }

    /**
     * Gets the information for a concrete specie.
     *
     * @param specie   The selected specie.
     * @param callback The callback.
     */
    void execute(String specie, Callback callback);

}
