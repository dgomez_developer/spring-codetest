package com.spring.deboragomez.species.domain.interactors.species;

import com.spring.deboragomez.species.domain.executors.PostExecutionThread;
import com.spring.deboragomez.species.domain.executors.ThreadExecutor;
import com.spring.deboragomez.species.domain.interactors.BaseInteractor;
import com.spring.deboragomez.species.domain.model.Animals;
import com.spring.deboragomez.species.domain.repository.SpeciesRepository;

import javax.inject.Inject;

/**
 * @author Débora Gómez Bertoli.
 */
public class GetSpeciesInteractorImpl extends BaseInteractor implements GetSpeciesInteractor {

    private SpeciesRepository repository;
    private Callback callback;


    /**
     * Constructor.
     *
     * @param threadExecutor      The thread executor.
     * @param postExecutionThread The post execution thread.
     * @param repository          The species repository.
     */
    @Inject
    public GetSpeciesInteractorImpl(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, SpeciesRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    public void execute(Callback callback) {
        this.callback = callback;
        execute();
    }

    @Override
    public void run() {

        repository.getSpecies(false, new SpeciesRepository.Callback() {
            @Override
            public void onSuccess(final Animals animals) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(animals.getFavourites(), animals.getOthers());
                    }
                });
            }

            @Override
            public void onError(final Exception e) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onError(e);
                    }
                });
            }
        });

    }
}
