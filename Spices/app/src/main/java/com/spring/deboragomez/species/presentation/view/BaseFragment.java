package com.spring.deboragomez.species.presentation.view;

import android.support.v4.app.Fragment;

import com.spring.deboragomez.species.presentation.di.HasComponent;

/**
 * Base class for fragments.
 *
 * @author Débora Gómez Bertoli.
 */
public class BaseFragment extends Fragment {

    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}
