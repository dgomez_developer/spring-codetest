package com.spring.deboragomez.species.presentation.view.adapters;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spring.deboragomez.species.R;
import com.spring.deboragomez.species.presentation.model.SpeciesCardView;

import java.util.List;

/**
 * Adapter fills in the card information.
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesCardsAdapter extends RecyclerView.Adapter<SpeciesCardsAdapter.ViewHolder> {

    private static ItemListListener listener;
    private List<SpeciesCardView> list;

    private Context context;

    /**
     * Constructor.
     *
     * @param itemList List of items.
     */
    public SpeciesCardsAdapter(Context context, List<SpeciesCardView> itemList, ItemListListener listener) {
        this.context = context;
        this.list = itemList;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_species_card_item, parent, false);
        ViewHolder viewholder = new ViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getName());
        holder.family.setText(list.get(position).getFamily());
        Glide.with(context).load(list.get(position).getImageUrl()).centerCrop().into(holder.image);

        ViewCompat.setTransitionName(holder.image, String.valueOf(position) + "_image");

        holder.ripple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpeciesCardsAdapter.listener.onItemClick(holder, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public SpeciesCardView getItemAt(int position) {
        return list.get(position);
    }

    /**
     * Listener called when clicking a item of the list
     */
    public interface ItemListListener {

        /**
         * Called after clicking an item.
         *
         * @param holder   The view holder.
         * @param position Item position within the list.
         */
        void onItemClick(ViewHolder holder, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public ImageView image;
        public TextView family;
        public View ripple;

        /**
         * Constructor.
         *
         * @param itemView List item view.
         */
        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.fragment_spices_card_name_textview);
            image = (ImageView) itemView.findViewById(R.id.imageview);
            family = (TextView) itemView.findViewById(R.id.fragment_spices_card_family_textview);
            ripple = (View) itemView.findViewById(R.id.rippleView);
        }
    }

}
