package com.spring.deboragomez.species.presentation.presenter;

import com.spring.deboragomez.species.presentation.view.SpeciesDetailView;

/**
 * The presenter interface for the Species details screen.
 *
 * @author Débora Gómez Bertoli.
 */
public interface SpeciesDetailPresenter {

    /**
     * Inits the screen information with the species details.
     *
     * @param species The species.
     */
    void onCreate(String species);

    /**
     * UI listener.
     *
     * @param speciesDetailFragment the fragment
     */
    void setView(SpeciesDetailView speciesDetailFragment);
}
