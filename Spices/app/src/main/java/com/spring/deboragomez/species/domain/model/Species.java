package com.spring.deboragomez.species.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The species model.
 *
 * @author Débora Gómez Bertoli.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Animals"
})
public class Species {

    @JsonProperty("Animals")
    private Animals animals;

    /**
     * @return The animals
     */
    @JsonProperty("Animals")
    public Animals getAnimals() {
        return animals;
    }

    /**
     * @param animals The Animals
     */
    @JsonProperty("Animals")
    public void setAnimals(Animals animals) {
        this.animals = animals;
    }

}
