package com.spring.deboragomez.species.presentation.view;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spring.deboragomez.species.R;
import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.presentation.di.components.SpeciesComponent;
import com.spring.deboragomez.species.presentation.presenter.SpeciesDetailPresenterImpl;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Use the {@link SpeciesDetailFragment#newInstance} factory method to
 * create an instance of this fragment to show the species details.
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesDetailFragment extends BaseFragment implements SpeciesDetailView {


    private static final String SPECIES_PARAM = "com.spring.debora.gomez.spices.SpeciesDetailFragment.SPECIES_PARAM";
    public static final String TAG = "SpeciesDetailFragment";

    private String species;

    @Inject
    SpeciesDetailPresenterImpl speciesPresenter;

    @Bind(R.id.imageview)
    ImageView image;

    @Bind(R.id.fragment_species_details_name_textview)
    TextView name;

    @Bind(R.id.fragment_species_details_family_textview)
    TextView family;

    @Bind(R.id.fragment_species_details_iucn_textview)
    TextView iucn;

    @Bind(R.id.fragment_species_details_year_textview)
    TextView year;

    @Bind(R.id.fragment_species_details_notes_textview)
    TextView notes;

    /**
     * Constructor.
     */
    public SpeciesDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SpeciesDetailFragment.
     */
    public static SpeciesDetailFragment newInstance(String species) {
        SpeciesDetailFragment fragment = new SpeciesDetailFragment();
        Bundle args = new Bundle();
        args.putString(SPECIES_PARAM, species);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(SpeciesComponent.class).inject(this);
        speciesPresenter.setView(this);
        if (getArguments() != null) {
            this.species = getArguments().getString(SPECIES_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_species_detail, container, false);
        ButterKnife.bind(this, view);
        setRetainInstance(true);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle saveInstanceBundle) {
        super.onActivityCreated(saveInstanceBundle);
        speciesPresenter.onCreate(species);
    }

    @Override
    public void showAnimalDetails(Animal animal) {
        Glide.with(getContext()).load(animal.getPicture()).centerCrop().into(image);
        name.setText(animal.getSpecies());
        family.setText(animal.getFamily());
        iucn.setText(animal.getIUCN());
        year.setText(animal.getYear());
        if (animal.getNotes().getCdata() != null && !animal.getNotes().getCdata().isEmpty()) {
            notes.setText(getString(R.string.fragment_details_notes_label, animal.getNotes().getCdata()));
        } else {
            notes.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(int errorId) {
        Snackbar.make(getView(), getString(errorId), Snackbar.LENGTH_LONG).show();
    }
}
