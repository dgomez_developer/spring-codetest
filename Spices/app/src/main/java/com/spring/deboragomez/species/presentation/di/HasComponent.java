package com.spring.deboragomez.species.presentation.di;

/**
 * Interface representing a contract for clients that contains a component for dependency injection.
 *
 * @author Débora Gómez Bertoli.
 */
public interface HasComponent<C> {

    C getComponent();
}
