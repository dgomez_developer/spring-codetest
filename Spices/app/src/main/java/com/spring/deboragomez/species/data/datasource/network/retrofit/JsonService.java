package com.spring.deboragomez.species.data.datasource.network.retrofit;

import com.spring.deboragomez.species.domain.model.Species;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Interface for Json requests.
 *
 * @author Débora Gómez Bertoli.
 */
public interface JsonService {


    /**
     * Gets the species from the server.
     * @return the request response.
     */
    @GET("test.json")
    Call<Species> getSpecies();
}
