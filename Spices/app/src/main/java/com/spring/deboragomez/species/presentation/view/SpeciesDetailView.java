package com.spring.deboragomez.species.presentation.view;

import com.spring.deboragomez.species.domain.model.Animal;

/**
 * {@link SpeciesDetailFragment} interface.
 *
 * @author Débora Gómez Bertoli.
 */
public interface SpeciesDetailView {

    /**
     * Shows the details for a concrete species.
     *
     * @param animal The animal.
     */
    void showAnimalDetails(Animal animal);

    /**
     * Shows a generic error on the screen.
     *
     * @param errorId The resource id message.
     */
    void showError(int errorId);
}
