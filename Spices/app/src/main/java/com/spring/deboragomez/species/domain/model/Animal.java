package com.spring.deboragomez.species.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Animal model.
 * @author Débora Gómez Bertoli.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "species",
        "family",
        "IUCN",
        "year",
        "picture",
        "notes"
})
public class Animal {

    @JsonProperty("species")
    private String species;
    @JsonProperty("family")
    private String family;
    @JsonProperty("IUCN")
    private String iUCN;
    @JsonProperty("year")
    private String year;
    @JsonProperty("picture")
    private String picture;
    @JsonProperty("notes")
    private Notes notes;

    /**
     * @return The species
     */
    @JsonProperty("species")
    public String getSpecies() {
        return species;
    }

    /**
     * @param species The species
     */
    @JsonProperty("species")
    public void setSpecies(String species) {
        this.species = species;
    }

    /**
     * @return The family
     */
    @JsonProperty("family")
    public String getFamily() {
        return family;
    }

    /**
     * @param family The family
     */
    @JsonProperty("family")
    public void setFamily(String family) {
        this.family = family;
    }

    /**
     * @return The iUCN
     */
    @JsonProperty("IUCN")
    public String getIUCN() {
        return iUCN;
    }

    /**
     * @param iUCN The IUCN
     */
    @JsonProperty("IUCN")
    public void setIUCN(String iUCN) {
        this.iUCN = iUCN;
    }

    /**
     * @return The year
     */
    @JsonProperty("year")
    public String getYear() {
        return year;
    }

    /**
     * @param year The year
     */
    @JsonProperty("year")
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return The picture
     */
    @JsonProperty("picture")
    public String getPicture() {
        return picture;
    }

    /**
     * @param picture The picture
     */
    @JsonProperty("picture")
    public void setPicture(String picture) {
        this.picture = picture;
    }

    /**
     * @return The notes
     */
    @JsonProperty("notes")
    public Notes getNotes() {
        return notes;
    }

    /**
     * @param notes The notes
     */
    @JsonProperty("notes")
    public void setNotes(Notes notes) {
        this.notes = notes;
    }

}
