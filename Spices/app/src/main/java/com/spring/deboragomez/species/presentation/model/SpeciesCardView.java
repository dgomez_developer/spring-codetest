package com.spring.deboragomez.species.presentation.model;

/**
 * Model for CardView.
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesCardView {

    private String imageUrl;

    private String name;

    private String family;

    /**
     * Constructor.
     *
     * @param species The species.
     * @param family  The family.
     * @param picture The picture URL.
     */
    public SpeciesCardView(String species, String family, String picture) {
        this.name = species;
        this.family = family;
        this.imageUrl = picture;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
