package com.spring.deboragomez.species.presentation.di.modules;

import com.spring.deboragomez.species.domain.interactors.species.GetSpecieInteractor;
import com.spring.deboragomez.species.domain.interactors.species.GetSpecieInteractorImpl;
import com.spring.deboragomez.species.domain.interactors.species.GetSpeciesInteractor;
import com.spring.deboragomez.species.domain.interactors.species.GetSpeciesInteractorImpl;
import com.spring.deboragomez.species.presentation.presenter.SpeciesDetailPresenter;
import com.spring.deboragomez.species.presentation.presenter.SpeciesDetailPresenterImpl;
import com.spring.deboragomez.species.presentation.presenter.SpeciesPresenter;
import com.spring.deboragomez.species.presentation.presenter.SpeciesPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * A module that provides the presenter of welcome.
 *
 * @author Débora Gómez Bertoli.
 */
@Module
public class SpeciesModule {

    @Provides
    public SpeciesPresenter provideSpeciesPresenterImpl(SpeciesPresenterImpl presenter) {
        return presenter;
    }

    @Provides
    public SpeciesDetailPresenter provideSpeciesDetailPresenterImpl(SpeciesDetailPresenterImpl presenter) {
        return presenter;
    }

    @Provides
    public GetSpeciesInteractor provideSpeciesInteractorImpl(GetSpeciesInteractorImpl interactor) {
        return interactor;
    }


    @Provides
    public GetSpecieInteractor provideSpecieInteractorImpl(GetSpecieInteractorImpl interactor) {
        return interactor;
    }
}