package com.spring.deboragomez.species.domain.interactors.species;

import com.spring.deboragomez.species.domain.model.Animal;

import java.util.List;

/**
 * Gets species information interactor interface.
 *
 * @author Débora Gómez Bertoli.
 */
public interface GetSpeciesInteractor {

    /**
     * The callback for the operation.
     */
    interface Callback {

        /**
         * Called when the request finishes.
         *
         * @param favAnimals   The favourite animals.
         * @param otherAnimals The other animals.
         */
        void onSuccess(List<Animal> favAnimals, List<Animal> otherAnimals);

        /**
         * Called when an error happens.
         *
         * @param e The error.
         */
        void onError(Exception e);

    }

    /**
     * Gets the species information.
     *
     * @param callback The callback.
     */
    void execute(Callback callback);

}
