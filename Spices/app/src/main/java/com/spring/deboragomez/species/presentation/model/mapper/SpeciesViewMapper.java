package com.spring.deboragomez.species.presentation.model.mapper;

import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.presentation.model.SpeciesCardView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Maps the business model into a view model.
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesViewMapper {

    @Inject
    public SpeciesViewMapper() {

    }

    /**
     * Gets favourites and others into a single list of CardView model.
     *
     * @param favourites The favourite animals.
     * @param others     The other animals.
     * @return The list of card view model animals.
     */
    public List<SpeciesCardView> map(List<Animal> favourites, List<Animal> others) {

        List<SpeciesCardView> items = new ArrayList<>();

        for (Animal fav : favourites) {
            //Fix for the malformed url of G. gorilla picture.
            String url;
            if (fav.getSpecies().equals("G. gorilla")) {
                url = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Male_gorilla_in_SF_zoo.jpg/800px-Male_gorilla_in_SF_zoo.jpg";
            } else {
                url = fav.getPicture();
            }

            SpeciesCardView item = new SpeciesCardView(fav.getSpecies(), fav.getFamily(), url);
            items.add(item);
        }

        for (Animal other : others) {
            //Fix for the malformed url of Felis catus picture.
            String url;
            if (other.getSpecies().equals("Felis catus")) {
                url = "https://upload.wikimedia.org/wikipedia/commons/e/e7/Jammlich_crop.jpg";
            } else {
                url = other.getPicture();
            }
            SpeciesCardView item = new SpeciesCardView(other.getSpecies(), other.getFamily(), url);
            items.add(item);
        }

        return items;
    }

    /**
     * Fixes the URL for Gorilla and Felis catus species.
     *
     * @param animal The animal.
     */
    public void fixImageUrlIfNeeded(Animal animal) {
        if (animal.getSpecies().equals("G. gorilla")) {
            animal.setPicture("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Male_gorilla_in_SF_zoo.jpg/800px-Male_gorilla_in_SF_zoo.jpg");
        } else if (animal.getSpecies().equals("Felis catus")) {
            animal.setPicture("https://upload.wikimedia.org/wikipedia/commons/e/e7/Jammlich_crop.jpg");
        } else {
            //Do nothing
        }
    }
}
