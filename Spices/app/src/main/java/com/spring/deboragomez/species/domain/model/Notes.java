package com.spring.deboragomez.species.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The animals extra information model.
 * @author Débora Gómez Bertoli.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "__cdata"
})
public class Notes {

    @JsonProperty("__cdata")
    private String cdata;

    /**
     * @return The cdata
     */
    @JsonProperty("__cdata")
    public String getCdata() {
        return cdata;
    }

    /**
     * @param cdata The cdata
     */
    @JsonProperty("__cdata")
    public void setCdata(String cdata) {
        this.cdata = cdata;
    }

}
