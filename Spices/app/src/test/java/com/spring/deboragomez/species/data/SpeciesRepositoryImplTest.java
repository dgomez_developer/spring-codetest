package com.spring.deboragomez.species.data;

import com.spring.deboragomez.species.SpeciesMockBuilder;
import com.spring.deboragomez.species.data.datasource.AnimalsDataSource;
import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.domain.model.Animals;
import com.spring.deboragomez.species.domain.model.Species;
import com.spring.deboragomez.species.domain.repository.SpeciesRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Tests for class {@link SpeciesRepositoryImpl}
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesRepositoryImplTest {


    private static final String MOCK_SPECIES = "Gorilla";

    private SpeciesRepositoryImpl repositoryUnderTest;

    private AnimalsDataSource mockNetworkDataSource;

    private AnimalsDataSource mockMemoryDataSource;

    private SpeciesRepository.Callback mockSpeciesCallback;

    private SpeciesRepository.SpecieCallback mockSpecieCallback;

    @Before
    public void setup() {
        mockSpecieCallback = mock(SpeciesRepository.SpecieCallback.class);
        mockSpeciesCallback = mock(SpeciesRepository.Callback.class);
        mockNetworkDataSource = mock(AnimalsDataSource.class);
        mockMemoryDataSource = mock(AnimalsDataSource.class);
        repositoryUnderTest = new SpeciesRepositoryImpl(mockMemoryDataSource, mockNetworkDataSource);
    }


    @Test
    public void whenGettingTheSpeciesAndNoCacheThenGetInfoFromServer() {

        mockSuccessfulServerRequest();

        repositoryUnderTest.getSpecies(false, mockSpeciesCallback);

        verify(mockMemoryDataSource).getSpecies(any(AnimalsDataSource.Callback.class));
        verify(mockNetworkDataSource).getSpecies(any(AnimalsDataSource.Callback.class));
        verify(mockMemoryDataSource).setSpecies(any(Species.class), any(AnimalsDataSource.Callback.class));
        verify(mockSpeciesCallback).onSuccess(any(Animals.class));

    }

    @Test
    public void whenGettingTheSpeciesAndThereIsCacheThenReturnCachedInformation() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AnimalsDataSource.Callback callback = (AnimalsDataSource.Callback) invocation.getArguments()[0];
                callback.onSuccess(mock(Species.class));
                return null;
            }
        }).when(mockMemoryDataSource).getSpecies(any(AnimalsDataSource.Callback.class));

        repositoryUnderTest.getSpecies(false, mockSpeciesCallback);

        verify(mockMemoryDataSource).getSpecies(any(AnimalsDataSource.Callback.class));
        verify(mockNetworkDataSource, never()).getSpecies(any(AnimalsDataSource.Callback.class));
        verify(mockSpeciesCallback).onSuccess(any(Animals.class));

    }

    @Test
    public void whenGettingTheSpeciesAndNoCacheThenErrorOccursGettingInfoFromServer() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AnimalsDataSource.Callback callback = (AnimalsDataSource.Callback) invocation.getArguments()[0];
                callback.onSuccess(null);
                return null;
            }
        }).when(mockMemoryDataSource).getSpecies(any(AnimalsDataSource.Callback.class));

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AnimalsDataSource.Callback callback = (AnimalsDataSource.Callback) invocation.getArguments()[0];
                callback.onError(mock(Exception.class));
                return null;
            }
        }).when(mockNetworkDataSource).getSpecies(any(AnimalsDataSource.Callback.class));

        repositoryUnderTest.getSpecies(false, mockSpeciesCallback);

        verify(mockMemoryDataSource).getSpecies(any(AnimalsDataSource.Callback.class));
        verify(mockNetworkDataSource).getSpecies(any(AnimalsDataSource.Callback.class));
        verify(mockMemoryDataSource, never()).setSpecies(any(Species.class), any(AnimalsDataSource.Callback.class));
        verify(mockSpeciesCallback, never()).onSuccess(any(Animals.class));
        verify(mockSpeciesCallback).onError(any(Exception.class));
    }


    @Test
    public void whenGettingASpeciesThenReturnResult() {

        mockSuccessfulServerRequest();

        repositoryUnderTest.getSpecie(MOCK_SPECIES, mockSpecieCallback);

        verify(mockMemoryDataSource).getSpecies(any(AnimalsDataSource.Callback.class));
        verify(mockNetworkDataSource).getSpecies(any(AnimalsDataSource.Callback.class));
        verify(mockMemoryDataSource).setSpecies(any(Species.class), any(AnimalsDataSource.Callback.class));
        ArgumentCaptor<Animal> captor = ArgumentCaptor.forClass(Animal.class);
        verify(mockSpecieCallback).onSuccess(captor.capture());
        Animal animal = captor.getValue();
        assertNotNull(animal);
        assertEquals(MOCK_SPECIES, animal.getSpecies());
    }

    private void mockSuccessfulServerRequest() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AnimalsDataSource.Callback callback = (AnimalsDataSource.Callback) invocation.getArguments()[0];
                callback.onSuccess(null);
                return null;
            }
        }).when(mockMemoryDataSource).getSpecies(any(AnimalsDataSource.Callback.class));

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AnimalsDataSource.Callback callback = (AnimalsDataSource.Callback) invocation.getArguments()[0];
                callback.onSuccess(mock(Species.class));
                return null;
            }
        }).when(mockNetworkDataSource).getSpecies(any(AnimalsDataSource.Callback.class));

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AnimalsDataSource.Callback callback = (AnimalsDataSource.Callback) invocation.getArguments()[1];
                callback.onSuccess(SpeciesMockBuilder.createMockSpeciesData(MOCK_SPECIES));
                return null;
            }
        }).when(mockMemoryDataSource).setSpecies(any(Species.class), any(AnimalsDataSource.Callback.class));
    }

}
