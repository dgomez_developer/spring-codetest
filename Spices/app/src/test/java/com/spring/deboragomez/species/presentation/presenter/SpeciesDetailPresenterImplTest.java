package com.spring.deboragomez.species.presentation.presenter;

import com.spring.deboragomez.species.R;
import com.spring.deboragomez.species.domain.interactors.species.GetSpecieInteractor;
import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.presentation.model.mapper.SpeciesViewMapper;
import com.spring.deboragomez.species.presentation.view.SpeciesDetailView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Tests for class {@link SpeciesDetailPresenterImpl}
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesDetailPresenterImplTest {


    private SpeciesDetailPresenterImpl presenterUnderTest;

    private GetSpecieInteractor mockInteractor;

    private SpeciesDetailView mockView;

    private SpeciesViewMapper mockMapper;

    @Before
    public void setup() {
        mockMapper = mock(SpeciesViewMapper.class);
        mockInteractor = mock(GetSpecieInteractor.class);
        mockView = mock(SpeciesDetailView.class);
        presenterUnderTest = new SpeciesDetailPresenterImpl(mockInteractor, mockMapper);
        presenterUnderTest.setView(mockView);
    }

    @Test
    public void whenGettingSpeciesInfoThenReturnedTheInfoFound() {

        final Animal animal = mock(Animal.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                GetSpecieInteractor.Callback callback = (GetSpecieInteractor.Callback) invocation.getArguments()[1];
                callback.onSuccess(animal);
                return null;
            }
        }).when(mockInteractor).execute(anyString(), any(GetSpecieInteractor.Callback.class));

        presenterUnderTest.onCreate("Mouse");

        verify(mockInteractor).execute(eq("Mouse"), any(GetSpecieInteractor.Callback.class));
        verify(mockView).showAnimalDetails(eq(animal));
    }

    @Test
    public void whenGettingSpeciesInfoAnErrorHappensTheShowAnError() {

        final Animal animal = mock(Animal.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                GetSpecieInteractor.Callback callback = (GetSpecieInteractor.Callback) invocation.getArguments()[1];
                callback.onError(new Exception("Error"));
                return null;
            }
        }).when(mockInteractor).execute(anyString(), any(GetSpecieInteractor.Callback.class));

        presenterUnderTest.onCreate("Mouse");

        verify(mockInteractor).execute(eq("Mouse"), any(GetSpecieInteractor.Callback.class));
        verify(mockView, never()).showAnimalDetails(eq(animal));
        verify(mockView).showError(R.string.generic_error);
    }
}
