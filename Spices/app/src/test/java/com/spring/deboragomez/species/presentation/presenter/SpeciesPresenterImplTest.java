package com.spring.deboragomez.species.presentation.presenter;

import com.spring.deboragomez.species.R;
import com.spring.deboragomez.species.domain.interactors.species.GetSpeciesInteractor;
import com.spring.deboragomez.species.presentation.model.mapper.SpeciesViewMapper;
import com.spring.deboragomez.species.presentation.view.SpeciesView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Tests for class {@link SpeciesPresenterImpl}
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesPresenterImplTest {

    private SpeciesPresenterImpl presenterUnderTest;

    private GetSpeciesInteractor mockInteractor;

    private SpeciesView mockView;

    private SpeciesViewMapper mockMapper;

    @Before
    public void setup() {
        mockMapper = mock(SpeciesViewMapper.class);
        mockView = mock(SpeciesView.class);
        mockInteractor = mock(GetSpeciesInteractor.class);
        presenterUnderTest = new SpeciesPresenterImpl(mockInteractor, mockMapper);
        presenterUnderTest.setView(mockView);
    }

    @Test
    public void whenGettingSpeciesInfoThenReturnTheInfoFound() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                GetSpeciesInteractor.Callback callback = (GetSpeciesInteractor.Callback) invocation.getArguments()[0];
                callback.onSuccess(mock(List.class), mock(List.class));
                return null;
            }
        }).when(mockInteractor).execute(any(GetSpeciesInteractor.Callback.class));

        presenterUnderTest.onCreate();

        verify(mockInteractor).execute(any(GetSpeciesInteractor.Callback.class));
        verify(mockMapper).map(anyList(), anyList());
        verify(mockView).showSpicesList(anyList());
    }

    @Test
    public void whenGettingSpeciesInfoAnErrorHappensThenShowAnError() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                GetSpeciesInteractor.Callback callback = (GetSpeciesInteractor.Callback) invocation.getArguments()[0];
                callback.onError(new Exception("Error"));
                return null;
            }
        }).when(mockInteractor).execute(any(GetSpeciesInteractor.Callback.class));

        presenterUnderTest.onCreate();

        verify(mockInteractor).execute(any(GetSpeciesInteractor.Callback.class));
        verify(mockView).showError(R.string.generic_error);
    }

}
