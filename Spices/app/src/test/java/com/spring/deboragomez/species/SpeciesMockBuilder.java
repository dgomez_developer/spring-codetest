package com.spring.deboragomez.species;

import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.domain.model.Animals;
import com.spring.deboragomez.species.domain.model.Species;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates mock data for unit tests.
 *
 * @author Débora Gómez Bertoli.
 */
public class SpeciesMockBuilder {


    /**
     * Creates mock taht with a species.
     *
     * @param speciestext The species.
     * @return the mock data.
     */
    public static Species createMockSpeciesData(String speciesText) {
        Species species = new Species();
        Animals animals = new Animals();
        List<Animal> favs = new ArrayList<>();
        List<Animal> other = new ArrayList<>();
        Animal animal = new Animal();
        animal.setSpecies(speciesText);
        favs.add(animal);
        Animal otherAnimal = new Animal();
        otherAnimal.setSpecies("Shark");
        other.add(otherAnimal);
        animals.setFavourites(favs);
        animals.setOthers(other);
        species.setAnimals(animals);
        return species;
    }

}
