package com.spring.deboragomez.species.domain.interactors.species;

import com.spring.deboragomez.species.SpeciesMockBuilder;
import com.spring.deboragomez.species.domain.executors.SameThreadExecutor;
import com.spring.deboragomez.species.domain.model.Animal;
import com.spring.deboragomez.species.domain.repository.SpeciesRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Tests for class {@link GetSpecieInteractorImpl}
 *
 * @author Débora Gómez Bertoli.
 */
public class GetSpecieInteractorImplTest {


    private static final String MOCK_SPECIES = "Mouse";
    private GetSpecieInteractorImpl interactorUnderTest;

    private SpeciesRepository mockRepository;

    private GetSpecieInteractor.Callback mockCallback;

    @Before
    public void setup() {
        mockCallback = mock(GetSpecieInteractor.Callback.class);
        mockRepository = mock(SpeciesRepository.class);
        SameThreadExecutor executor = new SameThreadExecutor();
        interactorUnderTest = new GetSpecieInteractorImpl(executor, executor, mockRepository);
    }

    @Test
    public void whenGettingSpecieTheReturnTheSpecieFound() {

        final Animal[] mockAnimal = new Animal[1];

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {

                SpeciesRepository.SpecieCallback callback = (SpeciesRepository.SpecieCallback) invocation.getArguments()[1];
                mockAnimal[0] = SpeciesMockBuilder.createMockSpeciesData(MOCK_SPECIES).getAnimals().getFavourites().get(0);
                callback.onSuccess(mockAnimal[0]);
                return null;
            }
        }).when(mockRepository).getSpecie(anyString(), any(SpeciesRepository.SpecieCallback.class));

        interactorUnderTest.execute(MOCK_SPECIES, mockCallback);

        verify(mockRepository).getSpecie(eq(MOCK_SPECIES), any(SpeciesRepository.SpecieCallback.class));
        verify(mockCallback).onSuccess(eq(mockAnimal[0]));
    }

    @Test
    public void whenGettingSpecieAndNoSpecieFoundThenReturnError() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {

                SpeciesRepository.SpecieCallback callback = (SpeciesRepository.SpecieCallback) invocation.getArguments()[1];
                callback.onSuccess(null);
                return null;
            }
        }).when(mockRepository).getSpecie(anyString(), any(SpeciesRepository.SpecieCallback.class));

        interactorUnderTest.execute(MOCK_SPECIES, mockCallback);

        verify(mockRepository).getSpecie(eq(MOCK_SPECIES), any(SpeciesRepository.SpecieCallback.class));
        verify(mockCallback).onError(any(Exception.class));
    }

    @Test
    public void whenGettingSpecieAndAnErrorHappensThenReturnError() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {

                SpeciesRepository.SpecieCallback callback = (SpeciesRepository.SpecieCallback) invocation.getArguments()[1];
                callback.onError(mock(Exception.class));
                return null;
            }
        }).when(mockRepository).getSpecie(anyString(), any(SpeciesRepository.SpecieCallback.class));

        interactorUnderTest.execute(MOCK_SPECIES, mockCallback);

        verify(mockRepository).getSpecie(eq(MOCK_SPECIES), any(SpeciesRepository.SpecieCallback.class));
        verify(mockCallback).onError(any(Exception.class));
    }

}
