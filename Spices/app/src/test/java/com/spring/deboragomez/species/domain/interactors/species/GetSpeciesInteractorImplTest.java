package com.spring.deboragomez.species.domain.interactors.species;

import com.spring.deboragomez.species.SpeciesMockBuilder;
import com.spring.deboragomez.species.domain.executors.SameThreadExecutor;
import com.spring.deboragomez.species.domain.model.Animals;
import com.spring.deboragomez.species.domain.repository.SpeciesRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Tests for class {@link GetSpeciesInteractorImpl}
 *
 * @author Débora Gómez Bertoli.
 */
public class GetSpeciesInteractorImplTest {

    private static final String MOCK_SPECIES = "Canary";
    private SpeciesRepository mockRepository;

    private GetSpeciesInteractorImpl interactorUnderTest;

    private GetSpeciesInteractor.Callback mockCallback;

    @Before
    public void setup() {
        mockCallback = mock(GetSpeciesInteractor.Callback.class);
        mockRepository = mock(SpeciesRepository.class);
        SameThreadExecutor executor = new SameThreadExecutor();
        interactorUnderTest = new GetSpeciesInteractorImpl(executor, executor, mockRepository);
    }


    @Test
    public void whenGettingSpeciesTheReturnTheSpeciesFound() {

        final Animals[] animals = new Animals[1];
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {

                SpeciesRepository.Callback callback = (SpeciesRepository.Callback) invocation.getArguments()[1];
                animals[0] = SpeciesMockBuilder.createMockSpeciesData(MOCK_SPECIES).getAnimals();
                callback.onSuccess(animals[0]);
                return null;
            }
        }).when(mockRepository).getSpecies(anyBoolean(), any(SpeciesRepository.Callback.class));

        interactorUnderTest.execute(mockCallback);

        verify(mockRepository).getSpecies(eq(false), any(SpeciesRepository.Callback.class));
        verify(mockCallback).onSuccess(eq(animals[0].getFavourites()), eq(animals[0].getOthers()));
    }

    @Test
    public void whenGettingSpeciesAndAnErrorHappensThenReturnError() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {

                SpeciesRepository.Callback callback = (SpeciesRepository.Callback) invocation.getArguments()[1];
                callback.onError(new Exception("Error"));
                return null;
            }
        }).when(mockRepository).getSpecies(anyBoolean(), any(SpeciesRepository.Callback.class));

        interactorUnderTest.execute(mockCallback);

        verify(mockRepository).getSpecies(eq(false), any(SpeciesRepository.Callback.class));
        verify(mockCallback).onError(any(Exception.class));
    }
}
